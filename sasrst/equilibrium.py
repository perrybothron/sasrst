"""
Functions and a script to determine the semi-analytic solution of
radiative shock waves in 1-D.
It is intended for use as a comparison to the results from a numerical
radiation hydrodynamics simulation code (e.g., AZEuS).

Follows Lowrie & Rauenzahn (2007, Shock Waves, 16, 445) for the
*equilibrium* radiation diffusion case.

"""

import numpy as np
from matplotlib import ticker

def radiationmodified_sound_speed (P0, gamma):
    """
    Calculate the radiation-modified sound speed.
    Note that this implementation assumes dimensionless units (following
    Lowrie & Rauenzahn), where :math:`\rho_0 = T_0 = 1`.  The notation used
    comes from the same paper.
    
    The sound speed takes the form (eqn. 11 of L&R):
    
    ..math::  (a_0^*)^2 = 1 + \frac{4}{9}\cal{P}_0 (\gamma - 1)\frac{4\cal{P}_0 \gamma + 3(5 - 3\gamma)}{1 + 4\cal{P}_0 \gamma (\gamma - 1)}
    
    where
    
    ..math:`P_0 = \frac{a_\mathrm{R} T_0^4}{\rho_0 a_0^2}
    
    is the dimensionless radiation pressure constant, :math:`\gamma` is
    the ratio of specific heats, :math:`a_0` is the fluid sound speed.

    """

    gamm1 = gamma - 1.0
    return np.sqrt( 1.0 + (4.0/9.0)*P0 * gamm1
                        * (4.0 * P0 * gamma + 3.0 * (5.0 - 3.0 * gamma) )
                        / (1.0 + 4.0 * P0 * gamma * gamm1) )


def zeldovich_spike_props (M0, vp, Tp, rhop, gamma):
    """
    Calculate the density, speed, and temperature of the Zel'Dovich spike
    for a super-critical shock (i.e., when :math:`Mp = vp / Tp > 1`).

    Follows Lowrie & Rauenzahn, eqs. (27); all quantities are assumed to
    be dimensionless already.

    :math:`M_0` is the dimensionless upstream Mach number, :math:`v_p` is
    the precursor velocity, :math:`T_p` is the precursor temperature,
    :math:`\rho_p` is the precursor material density, and :math:`\gamma`
    is the ratio of specific heats.

    """

    Mp = vp / np.sqrt(Tp)
    rho_spike = rhop * ((gamma + 1.0) * Mp**2) / (2.0 + (gamma - 1.0) * Mp**2 )
    v_spike = M0 / rho_spike
    T_spike = ( Tp * ((1.0 - gamma + 2.0 * gamma * Mp**2) * (2.0 + (gamma - 1.0) * Mp**2))
                   / ((gamma + 1.0)**2 * Mp**2) )

    return rho_spike, v_spike, T_spike

def downstream_rho (T1, P0, gamma):
    """
    Determine the downstream density.  It is primarily a function of the
    downstream temperature, which is not generally known a priori.
    This function will be called several times by `downstream_T` during
    the root-finding process to determine the downstream temperature.  It
    will then be called one to determine the final downstream density.
    
    Eqn. (12) of Lowrie & Rauenzahn.

    """
        
    f1 = ( 3.0 * (gamma + 1.0) * (T1 - 1.0)
         - P0 * gamma * (gamma - 1.0) * (7.0 + T1**4) )
    f2 = 12.0 * (gamma - 1.0)**2 * T1 * (3.0 + gamma * P0 * (1.0 + 7.0 * T1**4) )

    # Quadratic eqn. with positive root chosen.
    rho1 = (f1 + np.sqrt(f1**2 + f2)) / (6.0 * (gamma - 1.0) * T1 )

    return rho1


def downstream_T (T1, P0, M0, gamma):
    """
    Determine the downstream temperature for a radiative shock in the
    equilibrium radiation diffusion approximation.
    
    Uses eqn. (13) of Lowrie & Rauenzahn for the downstream temperature,
    and eqn. (12) for the density (`rho1`).  The idea is to find the root
    of the eqn. returned by this function.

    All quantities are assumed dimensionless.

    :math:`T_1` is the dimensionless downstream temperature, or rather
    the current estimate/guess, :math:`P_0` is the dimensionless radiation
    pressure constant, :math:`\gamma` is the ratio of specific heats,
    :math:`M_0` is the dimensionless upstream Mach number.

    """

    rho1 = downstream_rho(T1, P0, gamma)

    a = gamma * P0 * rho1 * T1**4
    d = 3.0 * rho1**2 * T1
    e = -3.0 * rho1 - gamma * P0 * rho1 - 3.0 * gamma * (rho1 - 1.0) * M0**2

    return (a + d + e)


def precursor_rho (Tp, P0, M0, gamma):
    """
    Calculate the precursor density in the equilibrium radiation diffusion
    approximation, following Lowrie & Rauenzahn, eqns. (25) & (26).

    :math:`T_p` is the dimensionless *precursor* temperature, :math:`P_0`
    is the dimensionless radiation pressure constant, :math:`\gamma` is
    the ratio of specific heats, and :math:`M_0` is the dimensionless
    upstream Mach number.

    """

    m = 0.5 * (gamma * M0**2 + 1.0) + gamma * P0 * (1.0 - Tp**4) / 6.0

    return (m - np.sqrt(m**2 - gamma * Tp * M0**2)) / Tp

def derivs (x, T, arg):
    """
    Callable driver function for the RHS that we want to solve.
    Eqn. (24) of Lowrie & Rauenzahn.

    """

    # Unpack the arguments.
    M0, P0, kappa, gamma  = arg

    # Secondary functions.
    rho = precursor_rho(T, P0, M0, gamma)
    f3 = ( 6.0 * rho**2 * (T - 1.0) / (gamma - 1.0)
         + 3.0 * M0**2 * (1.0 - rho**2)
         + 8.0 * P0 * rho * (T**4 - rho) )

    # Return the evaluation of the RHS of the ODE.
    return M0 * f3 / (24.0 * kappa * rho**2 * T**3)

if __name__ == "__main__":

    from scipy.optimize import brentq
    from scipy.integrate import ode

    # Set default font characteristics.  LaTeX style fonts are preferred.
    plt.rc('text', usetex=True)
    plt.rc('font', **{'family':'serif','serif':['Computer Modern Roman']})
    plt.rc('font', size=15)
    plt.interactive(True)

    P0 = 1.0e-4
    kappa = 1.0e-4
    M0 = 4.0
    gamma = 5./3.
    Trange = (1.0, 100.0)

    dx0 = 5e-4
    xmin = -0.3
    
    pextent_rho = [0.9,4.0]
    pextent_T = [0.0, 8.0]
    xextent = [-0.25,0.25]

    # Determine the downstream values of the temperature and density.
    T_downstream = brentq(downstream_T, Trange[0], Trange[1], (P0, M0, gamma),
                          full_output=False)
    rho_downstream = downstream_rho(T_downstream, P0, gamma)

    print "T_downstream = {0:0.6f}, rho_downstream = {1:0.6f}".format(
        T_downstream, rho_downstream)

    # Calculate the downstream velocity (eqn. 7 of Lowrie & Rauenzahn).
    v_downstream = M0 / rho_downstream

    # Set up to integrate eqn. (24) of Lowrie & Rauenzahn.
    # The 'dopri5' integrator corresponds to a 4(5)th order Runge-Kutta.
    result = ode(derivs).set_integrator('dopri5')

    x_pre_min = 0.0
    result.set_initial_value(T_downstream, x_pre_min).set_f_params([M0,P0,kappa,gamma])

    soln = []
    k = 0
    # Perform numerical integration; the recipe is based on the `scipy`
    # manual page for `integrate.ode`.
    while result.successful() and result.t > xmin:
        result.integrate(result.t - dx0)
        soln.append([result.t, float(result.y)])
        k += 1

    print "Number of steps: {0}".format(k)
    soln = np.array(soln)
    x_precursor = soln[::-1,0].squeeze()
    T_precursor = soln[::-1,1].squeeze()

    # Calculate precursor density and velocity.
    rho_precursor = precursor_rho(T_precursor, P0, M0, gamma)
    v_precursor = M0 / rho_precursor

    # What about the Zel'dovich spike?
    nx = x_precursor.shape[0]
    M_precursor = v_precursor[nx-1] / np.sqrt(T_precursor[nx-1])
    if M_precursor > 1.0:
        rho_spike, v_spike, T_spike = zeldovich_spike_props(M0, v_precursor[nx-1],
            T_precursor[nx-1], rho_precursor[nx-1], gamma)

    # Plot the solution.
    fig1 = plt.figure("Equilibrium approx.",facecolor='w',figsize=(6.0,9.0))
    fig1.clf()
    fig1.subplots_adjust(bottom=0.06,top=0.93,hspace=0.06,left=0.11,right=0.95)
    ax1 = fig1.add_subplot(2,1,1)
    ax2 = fig1.add_subplot(2,1,2,sharex=ax1)

    titlestring = r"${\cal P}_0 = " + "{0:.2e}$; ".format(P0)
    titlestring += r"${\cal M}_0 = " + "{0:.2f}$; ".format(M0)
    titlestring += r"$\kappa = " + "{0:.2e}$; ".format(kappa)
    titlestring += r"$\gamma = " + "{0:.4f}$".format(gamma)
    fig1.suptitle(titlestring)

    # Set axes properties.
    ax1.set_ylabel(r"$\rho$")
    ax1.axis(ymin=pextent_rho[0],ymax=pextent_rho[1])
    ax1.xaxis.set_minor_locator(ticker.AutoMinorLocator())
    ax1.yaxis.set_minor_locator(ticker.AutoMinorLocator())

    ax2.set_ylabel(r"$T$")
    ax2.set_xlabel(r"$x$")
    ax2.axis(ymin=pextent_T[0],ymax=pextent_T[1],xmin=xextent[0],xmax=xextent[1])
    ax2.xaxis.set_minor_locator(ticker.AutoMinorLocator())
    ax2.yaxis.set_minor_locator(ticker.AutoMinorLocator())

    # Aesthetic tweaks.
    ax1.xaxis.set_tick_params(labelbottom='off')
    ax1.grid(True)
    ax2.grid(True)

    # Plot percursor region.
    ax1.plot(x_precursor,rho_precursor,'b-')
    ax2.plot(x_precursor,T_precursor,'r-')

    # Plot downstream region.
    x_down_max = 0.30
    ax1.plot([x_pre_min,x_down_max],[rho_downstream,rho_downstream],'b-')
    ax2.plot([x_pre_min,x_down_max],[T_downstream,T_downstream],'r-')

    # Transition between precursor and downstream regions (only visible in density).
    ax1.plot([x_precursor.max(),x_pre_min],[rho_precursor.max(),rho_downstream],'b-')

    # Plot Zel'dovich spike properties.
    if M_precursor > 1.0:
        ax1.plot([x_pre_min],[rho_spike],'ko',ms=5,mew=0)
        ax2.plot([x_pre_min],[T_spike],'ko',ms=5,mew=0)

    plt.draw()
    plt.savefig('equil_results.png')


