# -*- coding: utf-8 -*-
"""
Functions and a script to determine the semi-analytic solution of
radiative shock waves in 1-D.
It is intended for use as a comparison to the results from a numerical
radiation hydrodynamics simulation code (e.g., AZEuS).

Follows Lowrie & Edwards (2008, Shock Waves, 18, 129) for the
*non-equilibrium* radiation diffusion case.

Note that this script depends on `equilibrium.py`.

"""

import numpy as np
import matplotlib.pyplot as plt
from matplotlib import ticker
from equilibrium import downstream_rho, downstream_T


def Km_const (P0, M0, gamma):
    """
    Constant expression :math:`K_m` which shows up all over the place.
    Eqn. (16) from Lowrie & Edwards.

    """

    return 3.0 * (gamma * M0**2 + 1.0) + gamma * P0

def dTdtheta (P0, M0, kappa, sigma_a, gamma, M, T, theta, negbranch=True):
    """
    Calculate the value of :math:`dT / d\theta` for the upstream (0-state)
    or downstream (1-state) state, following Appendix A of Lowrie & Edwards.

    The challenge is that :math:`dT / dx = d\theta / dx = 0` and the
    differential eqns. for :math:`T` and :math:`x` are singular when the
    solution is constant.  Note that :math:`M` is the independent
    variable, while :math:`T` and :math:`\theta` are the dependent
    variables.
    
    :math:`P_0` is the dimensionless radiation pressure constant,
    :math:`M_0` is the dimensionless upstream Mach number,  :math:`\gamma`
    is the ratio of specific heats, :math:`\kappa` is the total extinction
    opacity, :math:`\sigma_a` is the absorption cross-section.
    
    The `negbranch` keyword determines the sign/branch of any quadratic
    expressions below.

    """

    Cp = 1.0 / (gamma - 1.0)

    # Logic to choose between branches of the quadratic expressions.
    if negbranch:
        pm = -1.0
    else:
        pm = 1.0

    rho = rho_branching(P0, M0, gamma, T, theta, negbranch=negbranch)
    v = M0 / rho

    # Eqns. (59) and (60).
    c1 = M0 / (24.0 * P0 * kappa * rho**2 * theta**3)
    c2 = P0 / (3.0 * Cp * M0 * (M**2 - 1.0))

    Km = Km_const(P0, M0, gamma)
    d = np.sqrt( (Km - gamma * P0 * theta**4)**2 - 36.0 * gamma * M0**2 * T )

    # Eqns. (61) and (62).
    drhodT = - (rho + pm * 3.0 * gamma * M0**2 / d) / T
    drhodtheta = ( -2.0 * P0 * gamma * theta**3 / (3.0 * T)
                 * (1.0 + pm * (Km - gamma * P0 * theta**4) / d) )

    # Eqns. (55)-(58)
    dGdT = c1 * ( 6.0 * Cp * rho * (2.0 * drhodT * (T - 1.0) + rho)
                - 6.0 * M0**2 * rho * drhodT
                + 8.0 * P0 * drhodT * (theta**4 - 2.0 * rho) )
    dGdtheta = c1 * ( 12.0 * Cp * drhodtheta * rho * (T - 1.0) 
                    - 6.0 * M0**2 * rho * drhodtheta
                    + 8.0 * P0 * (drhodtheta * (theta**4 - 2.0 * rho)
                                 + 4.0 * rho * theta**3) )
    dFdT = c2 * ( 4.0 * v * theta**3 * dGdT
                - 12.0 * sigma_a * T**3 * (gamma * M**2 - 1.0 ) )
    dFdtheta = c2 * ( 4.0 * v * theta**3 * dGdtheta
                    + 12.0 * sigma_a * theta**3 * (gamma * M**2 - 1.0) )

    # Eqn. (54).
    # Applies to both upstream and downstream regions (but using different
    # inputs).
    d = (dFdT - dGdtheta)**2 + 4.0 * dGdT * dFdtheta
    dTdtheta = ( ( dFdT - dGdtheta + pm * np.sqrt(d) ) / (2.0 * dGdT) )

    return dTdtheta


def rho_branching (P0, M0, gamma, T, theta, negbranch=True):
    """
    Calculate the density :math:`rho` using eqn. (17) of Lowrie & Edwards.

    This function is a quadratic equation, and therefore has two possible
    solutions.  The branch chosen depends on which state we want (negative
    or positive root) and the conditions at hand.

    """

    Km = Km_const (P0, M0, gamma)

    if negbranch:
        pm = -1.0
    else:
        pm = 1.0

    b = Km - gamma * P0 * theta**4
    d = b**2 - 36.0 * gamma * M0**2 * T
    rho = ( b + pm * np.sqrt(d) ) / (6.0 * T)

    return rho


def dthetadx (P0, M0, kappa, gamma, rho, v, T, theta):
    """
    Calculate eqn. (18) from Lowrie & Edwards.  It is needed to determine
    the value of :math:`x_0` (eqn. 44), and furthermore the precursor
    region structure.

    """

    Cp = 1.0 / (gamma - 1.0)

    dthetadx = v * ( 6.0 * Cp * rho * (T - 1.0)
                   + 3.0 * rho * (v**2 - M0**2)
                   + 8.0 * P0 * (theta**4 - rho) ) / (24.0 * P0 * kappa * theta**3)

    return dthetadx

def theta4TM (P0, M0, gamma, M, T):
    """
    Calculate eqn. (41) of Lowrie & Edwards to determine the radiation
    temperature (:math:`\theta = (E_\mathrm{R} / a_\mathrm{rad})**(1/4)`.

    """

    Km = Km_const (P0, M0, gamma)
    rho = M0 / (M * np.sqrt(T))

    return (Km - 3.0 * gamma * M0**2 / rho - 3.0 * T * rho) / (gamma * P0)

def derivs (M, y, arg):
    """
    Callable driver function for the RHS that we want to solve.
    Eqns. (37) and (38) of Lowrie & Edwards.
    
    :math:`y[0] = x`; :math:`y[1] = T`.

    """

    # Unpack the arguments.
    P0, M0, sigma_a, kappa, gamma  = arg

    # Secondary functions.
    Km = Km_const (P0, M0, gamma)  # eqn. (16)
    rho = M0 / (M * np.sqrt(y[1])) # eqn. (40)
    v = M * np.sqrt(y[1])          # eqn. (14)
    theta4 = theta4TM(P0, M0, gamma, M, y[1]) # eqn. (41)
    theta = theta4**0.25
    r = 3.0 * rho * sigma_a * (theta4 - y[1]**4) # eqn. (25)
    thetapri = dthetadx(P0, M0, kappa, gamma, rho, v, y[1], theta) # eqn. (18)
    ZN = 4.0 * M0 * theta**3 * thetapri + r * (gamma * M**2 - 1.0) # eqn. (24)
    ZD = ( 4.0 * M0 * theta**3 * thetapri + r * (gamma * M**2 + 1.0)
                                          * (gamma - 1.0) / (gamma + 1.0) ) # eqn. (39)

    dxdM = -6.0 * (M0 * rho * y[1] * (M**2 - 1.0)) / ((gamma + 1.0) * P0 * M * ZD)
    dTdM = -2.0 * ((gamma - 1.0) * y[1] * ZN) / ((gamma + 1.0) * M * ZD)

    # Return the results.
    return [dxdM, dTdM]

def jump_conditions (M, gamma):
    """
    Calculate the (Rankine-Hugoniot) hydrodynamic shock jump conditions
    for the density, velocity, and temperature.
    
    These relations can be found in many places, for e.g., in Clarke's
    MHD course notes (eqns. 2.32, 2.33, and 2.35).

    :math:`M` is the local Mach number, and :math:`\gamma` is the ratio
    of specific heats.

    """

    ratio_v = (M**2 * (gamma - 1.0) + 2) / (M**2 * (gamma + 1.0))
    ratio_rho = 1.0 / ratio_v
    ratio_T = 1.0 + 1.0 / (gamma + 1.0)**2 * ( 2.0 * (gamma - 1.0)
                                             * (gamma + 1.0/M**2) * (M**2 - 1) )

    return ratio_rho, ratio_T, ratio_v

def makeplot(axa, axb, axc, x_upstream, rho_upstream, T_upstream, theta_upstream,
    v_upstream, x_precursor, rho_precursor, T_precursor, theta_precursor,
    v_precursor, x_relax, rho_relax, T_relax, theta_relax, v_relax, x_downstream,
    rho_downstream, T_downstream, theta_downstream, v_downstream, drho_jump,
    dT_jump, dv_jump, indices, x_scale=1.0, rho_scale=1.0, T_scale=1.0,
    v_scale=1.0):
    """Plot the results, putting together the difference pieces/regions."""

    xoffset = 0.0
    ntotal = 512
    dx = (x_downstream - x_upstream) / ntotal
    x_upstream_a = np.arange(x_upstream,x_precursor.min()*x_scale,dx) + xoffset
    x_downstream_a = np.arange(x_relax.max()*x_scale,x_downstream,dx) + xoffset

    # Plot percursor region.
    axa.plot(x_precursor*x_scale,rho_precursor*rho_scale,'bo-',lw=1.5,ms=3,mew=0)
    axb.plot(x_precursor*x_scale,T_precursor*T_scale,'ro-',lw=1.5,ms=3,mew=0)
    axb.plot(x_precursor*x_scale,theta_precursor*T_scale,'go-',lw=1.5,ms=3,mew=0)
    axc.plot(x_precursor*x_scale,v_precursor*v_scale,'mo-',lw=1.5,ms=3,mew=0)

    # Plot relaxation region.
    axa.plot(x_relax*x_scale,rho_relax*rho_scale,'bo-',lw=1.5,ms=3,mew=0)
    axb.plot(x_relax*x_scale,T_relax*T_scale,'ro-',lw=1.5,ms=3,mew=0)
    axb.plot(x_relax*x_scale,theta_relax*T_scale,'go-',lw=1.5,ms=3,mew=0)
    axc.plot(x_relax*x_scale,v_relax*v_scale,'mo-',lw=1.5,ms=3,mew=0)

    # Plot upstream region.
    rho_upstream_a = np.ones_like(x_upstream_a) * rho_upstream * rho_scale
    T_upstream_a = np.ones_like(x_upstream_a) * T_upstream * T_scale
    theta_upstream_a = np.ones_like(x_upstream_a) * theta_upstream * T_scale
    v_upstream_a = np.ones_like(x_upstream_a) * v_upstream * v_scale
    axa.plot(x_upstream_a,rho_upstream_a,'bo-',lw=1.5,ms=3,mew=0)
    axb.plot(x_upstream_a,T_upstream_a,'ro-',lw=1.5,ms=3,mew=0)
    axb.plot(x_upstream_a,theta_upstream_a,'go-',lw=1.5,ms=3,mew=0)
    axc.plot(x_upstream_a,v_upstream_a,'mo-',lw=1.5,ms=3,mew=0)

    # Plot downstream region.
    rho_downstream_a = np.ones_like(x_downstream_a) * rho_downstream * rho_scale
    T_downstream_a = np.ones_like(x_downstream_a) * T_downstream * T_scale
    theta_downstream_a = np.ones_like(x_downstream_a) * theta_downstream * T_scale
    v_downstream_a = np.ones_like(x_downstream_a) * v_downstream * v_scale
    axa.plot(x_downstream_a,rho_downstream_a,'bo-',lw=1.5,ms=3,mew=0)
    axb.plot(x_downstream_a,T_downstream_a,'ro-',lw=1.5,ms=3,mew=0)
    axb.plot(x_downstream_a,theta_downstream_a,'go-',lw=1.5,ms=3,mew=0)
    axc.plot(x_downstream_a,v_downstream_a,'mo-',lw=1.5,ms=3,mew=0)

    if theta_precursor.max() < theta_relax.min():
        pass
    else:
        # Plot hydrodynamic shock region.
        rho_jump0 = rho_precursor[indices['th_pindx']]*rho_scale
        T_jump0 = T_precursor[indices['th_pindx']]*T_scale
        v_jump0 = v_precursor[indices['th_pindx']]*v_scale
        axa.plot([0.0,0.0],[rho_jump0,rho_jump0*drho_jump],'b-',lw=1.5)
        axb.plot([0.0,0.0],[T_jump0,T_jump0*dT_jump],'r-',lw=1.5)
        axc.plot([0.0,0.0],[v_jump0,v_jump0*dv_jump],'m-',lw=1.5)

    return None


if __name__ == "__main__":

    from scipy.optimize import brentq
    from scipy.integrate import ode

    # Set default font characteristics.  LaTeX style fonts are preferred.
    plt.rc('text', usetex=True)
    plt.rc('font', **{'family':'serif','serif':['Computer Modern Roman']})
    plt.rc('font', size=15)
    plt.interactive(True)

    # physical parameters
    P0 = 1.0e-4     # ratio of radiation energy to material energy
    sigma_a = 1.0e6 # absorption cross-section
    kappa = 1.0     # radiation diffusivity
    M0 = 2.0        # shock Mach number
    gamma = 5./3.   # ratio of specific heats

    # numerical integration parameters
    eps0 = 1.0e-7 # temperature perturbation applied in precursor region
    eps1 = -eps0  # temperature perturbation applied in relaxation region
    eps_asp = 1.0e-3
    dM = 5.0e-4
    drho_fudge = 1.0
    dT_fudge = 1.0
    dv_fudge = 1.0/drho_fudge

    lmakeplot = True
    lrealunits = True

    # M0 = 2.0 plotting parameters
    pextent_rho = [0.95,2.50]
    pextent_T = [0.95, 2.3]
    pextent_v = [0.7, 2.1]
    xextent = [-0.01,0.005]
    # M0 = 3.0 plotting parameters
    pextent_rho = [0.9,3.2]
    pextent_T = [0.9, 4.6]
    pextent_v = [0.9, 3.2]
    xextent = [-0.04,0.02]
    # M0 = 5.0 plotting parameters
    pextent_rho = [0.95,4.0]
    pextent_T = [0.0, 11.5]
    pextent_v = [1.2, 5.4]
    xextent = [-0.04,0.02]

    if lrealunits:
        # Scaling factors.
        L_tilde = 1.0e5 # [cm]
        T_tilde = 100.0 # [K]
        rho_tilde = 5.459625e-13 # [g cm**-3]
        a_tilde = 1.177184e5 # [cm s**-1]
        sigma_tilde = 3.926696e-5 # [cm**-1]
        kappa_tilde = 0.848896 # [cm**-1]

        pextent_rho = [x * rho_tilde for x in pextent_rho]
        pextent_T = [x * T_tilde for x in pextent_T]
        pextent_v = [x * a_tilde for x in pextent_v]
        xextent = [x * L_tilde for x in xextent]
    else:
        L_tilde = 1.0
        T_tilde = 1.0
        rho_tilde = 1.0
        a_tilde = 1.0
        sigma_tilde = 1.0
        kappa_tilde = 1.0

    theta0 = T0 = rho0 = 1.0
    v0 = M0

    print "Mach number M0 = {0}".format(M0)

    # Determine the downstream values of the temperature and density using
    # the equilibrium solution functions (`equilibrium.py`) and a numerical
    # integration.
    # I assume the root lies in between temperatures of 1 and 100. Note that
    # 1.0 is a trivial root to `downstream_T` when `rho0` equals one; To avoid
    # this possibility, the integration starts at `1.0 + eps0`.
    Trange = (1.0, 100.0)
    T_downstream, rresults = brentq(downstream_T, Trange[0] + eps0, Trange[1], (P0, M0, gamma),
                                    full_output=True, disp=True)
    rho_downstream = downstream_rho(T_downstream, P0, gamma)
    v_downstream = M0 / rho_downstream
    theta_downstream = T_downstream
    M_downstream = v_downstream / np.sqrt(T_downstream)

    # ---------- Precursor Region ----------

    # Determine the precursor 'epsilon' state.
    # This is used as the initial conditions for integrating eqns. (37)
    # and (38) for the full precursor region.
    # According to section 4.3, the -ve root always satisfies the upstream
    # conditions.
    theta_eps0 = theta0 + eps0
    T_eps0 = T0 + eps0 * dTdtheta(P0, M0, kappa, sigma_a, gamma, M0, T0, theta0,
                                  negbranch=True)
    rho_eps0 = rho_branching(P0, M0, gamma, T_eps0, theta_eps0, negbranch=True)
    v_eps0 = M0 / rho_eps0
    x_eps0 = 0.0 - eps0 / dthetadx(P0, M0, kappa, gamma, rho_eps0, v_eps0,
                                   T_eps0, theta_eps0)
    M_eps0 =  v_eps0/np.sqrt(T_eps0)

    # Numerically integrate across the precursor region (eqns. 37 and 38),
    # using the local Mach number :math:`M` as the independent variable.
    # The 'dopri5' integrator corresponds to a 4(5)th order Runge-Kutta;
    # 'dop853' corresponds to 8(5,3)th order.
    result0 = ode(derivs).set_integrator('dop853')
    args = [P0, M0, sigma_a, kappa, gamma]
    # `y[0] = x`; `y[1] = T`.
    y0 = [x_eps0, T_eps0]
    result0.set_initial_value(y0, M0).set_f_params(args)

    soln0 = []
    k0 = 0
    # Perform numerical integration; the recipe is based on the `scipy`
    # manual page for `integrate.ode`.
    # The independent variable `t` is, in fact, the Mach number, `M`.
    while result0.successful() and result0.t >= (1.0 + eps_asp):
        result0.integrate(result0.t - dM)
        k0 += 1
        theta = theta4TM(P0, M0, gamma, result0.t, float(result0.y[1]))**0.25
        soln0.append([result0.t, float(result0.y[0]), float(result0.y[1]), theta])
        if theta > T_downstream: 
            print "theta = {0} > T_downstream = {1}".format(theta, T_downstream)
            break

    print "Precursor number of steps: {0}".format(k0)
    soln0 = np.array(soln0)
    M_precursor = soln0[:,0].squeeze()
    x_precursor = soln0[:,1].squeeze()
    T_precursor = soln0[:,2].squeeze()
    theta_precursor = theta4TM(P0, M0, gamma, M_precursor, T_precursor)**0.25
    rho_precursor = rho_branching(P0, M0, gamma, T_precursor, theta_precursor,
                                  negbranch=True)
    v_precursor = M0 / rho_precursor
    slope_precursor = np.diff(theta_precursor) / np.diff(x_precursor)

    # ---------- Relaxation Region ----------

    # Determine the relaxation 'epsilon' state (in the same manner as before).
    # Which root of eqn. (17) (`rho_branching`, etc.) do we take?
    # --> The result of `rho_branching` should match `rho_downstream`.
    drho = rho_downstream - rho_branching(P0, M0, gamma, T_downstream,
                                          theta_downstream, negbranch=False)
    if np.abs(drho) > 1.0e-6:
      negroot = True
    else:
      negroot = False

    T1 = T_downstream
    theta1 = theta_downstream
    M1 = M_downstream
    theta_eps1 = theta1 + eps1
    T_eps1 = T1 + eps1 * dTdtheta(P0, M0, kappa, sigma_a, gamma, M1, T1, theta1,
                                  negbranch=negroot)
    rho_eps1 = rho_branching(P0, M0, gamma, T_eps1, theta_eps1, negbranch=negroot)
    v_eps1 = M0 / rho_eps1
    x_eps1 = -0.15 - eps1 / dthetadx(P0, M0, kappa, gamma, rho_eps1, v_eps1,
                                   T_eps1, theta_eps1)
    # Using `M1 = M_downstream` as initial conditions doesn't always work
    # for the relaxation region; define `M_eps1` from eqn. (14) and use
    # as an initial condition as necesary.
    M_eps1 = v_eps1 / np.sqrt(T_eps1)

    # Numerically integrate again, but this time for the relaxation region.
    # I tried the `dopri5` integrator first, but was having continual problems
    # with the numerical integration of the relaxation region.
    result1 = ode(derivs).set_integrator('dop853',nsteps=2000)
    args = [P0, M0, sigma_a, kappa, gamma]
    y0 = [x_eps1, T_eps1]
    if M0 >= 4.0:
        result1.set_initial_value(y0, M1).set_f_params(args)
    else:
        result1.set_initial_value(y0, M_eps1).set_f_params(args)

    soln1 = []
    k1 = 0
    # Perform numerical integration.
    while result1.successful() and result1.t <= (1.0 - eps_asp):
        result1.integrate(result1.t + dM)
        k1 += 1
        theta = theta4TM(P0, M0, gamma, result1.t, float(result1.y[1]))**0.25
        soln1.append([result1.t, float(result1.y[0]), float(result1.y[1]), theta])
        if theta < 1.0:
            print "theta < 1"
            break

    print "Relaxation number of steps: {0}".format(k1)
    soln1 = np.array(soln1)
    # since we integrated in the -ve x-direction, store the results in reverse order.
    M_relax = soln1[::-1,0].squeeze()
    x_relax = soln1[::-1,1].squeeze()
    T_relax = soln1[::-1,2].squeeze()
    theta_relax = theta4TM(P0, M0, gamma, M_relax, T_relax)**0.25
    rho_relax = rho_branching(P0, M0, gamma, T_relax, theta_relax,
                              negbranch=negroot)
    v_relax = M0 / rho_relax
    slope_relax = np.diff(theta_relax) / np.diff(x_relax)

    # ---------- Join the Regions Together ----------

    # Next, we need to join the precursor and relaxation regions together,
    # sometimes with a hydrodynamic shock.
    if theta_precursor.max() < theta_relax.max():
        print "Continuous case ({0} > {1}).".format(theta_precursor.max(), theta_relax.max())
        x_precursor_ma = np.ma.array(x_precursor,mask=False)
        x_relax_ma = np.ma.array(x_relax,mask=False)
        indices = None
        raise Exception("The implementation for the continuous case (without" +
                        " a hydrodynamic shock) is incomplete.") 
    else:
        print "Embedded shock case."

        # Try to find a location where both theta and dtheta / dx are
        # very similar in the precursor and relaxation region data.
        deltaindx = 5
        j = 0
        overlap = []
        output = []
        for th1,m1 in zip(theta_relax,slope_relax):
            delta_theta = np.abs(th1 - theta_precursor)
            delta_slope = np.abs(m1 - slope_precursor)
            theta_indx = np.where(delta_theta == delta_theta.min())[0][0]
            slope_indx = np.where(delta_slope == delta_slope.min())[0][0]
            output.append([theta_indx, slope_indx, th1, m1, theta_precursor[theta_indx], slope_precursor[slope_indx]])
            if np.abs(theta_indx - slope_indx) <= deltaindx:
                overlap.append({'rindx':j, 'th_pindx':theta_indx,'sl_pindx': slope_indx})
            j += 1

        # save results for debugging purposes
        output = np.array(output)
        
        # Translate the two regions so that they are continuous in `theta`.
        if len(overlap) > 0:
            indices = overlap[0]
        else:
            raise Exception("Overlapping indices not found; cannot translate regions.")
        xleft = x_precursor[indices['th_pindx']]
        xright = x_relax[indices['rindx']]
        print "precursor region translation: {0}; relaxation region translation: {1}".format(xleft,xright)
        x_precursor -= xleft
        x_relax -= xright

        # Next, remove solution before/after the overlap point in the
        # relaxation/precursor regions.
        x_precursor_ma = np.ma.array(x_precursor,mask=False)
        x_precursor_ma.mask[indices['th_pindx']:] = True
        x_relax_ma = np.ma.array(x_relax,mask=False)
        x_relax_ma.mask[:indices['rindx']] = True

        # Add a hydrodynamic shock with jump conditions given by the
        # Rankine-Hugoniot relations.
        M_jump = M_precursor[indices['th_pindx']]
        drho_jump, dT_jump, dv_jump = jump_conditions(M_jump, gamma)
        drho_jump *= drho_fudge
        dT_jump *= dT_fudge
        dv_jump *= dv_fudge

    # Plot the solution.
    if lmakeplot:
        fig1 = plt.figure("Non-equilibrium approx.",facecolor='w',figsize=(6.0,12.0))
        fig1.clf()
        fig1.subplots_adjust(bottom=0.04,top=0.92,hspace=0.0,left=0.15,right=0.95)
        ax1 = fig1.add_subplot(3,1,1)
        ax2 = fig1.add_subplot(3,1,2,sharex=ax1)
        ax3 = fig1.add_subplot(3,1,3,sharex=ax1)

        titlestring = r"${\cal P}_0 = " + "{0:.2e}$; ".format(P0)
        titlestring += r"${\cal M}_0 = " + "{0:.2f}$; ".format(M0)
        titlestring += r"$\kappa = " + "{0:.2e}$;\n ".format(kappa)
        titlestring += r"$\sigma_a = " + "{0:.2e}$; ".format(sigma_a)
        titlestring += r"$\gamma = " + "{0:.4f}$".format(gamma)
        fig1.suptitle(titlestring)

        # Set axes properties.
        ax1.set_ylabel(r"$\rho$")
        ax1.axis(ymin=pextent_rho[0],ymax=pextent_rho[1])
        ax1.xaxis.set_minor_locator(ticker.AutoMinorLocator())
        ax1.yaxis.set_minor_locator(ticker.AutoMinorLocator())

        ax2.set_ylabel(r"$T$")
        ax2.axis(ymin=pextent_T[0],ymax=pextent_T[1],xmin=xextent[0],xmax=xextent[1])
        ax2.xaxis.set_minor_locator(ticker.AutoMinorLocator())
        ax2.yaxis.set_minor_locator(ticker.AutoMinorLocator())
        
        ax3.set_ylabel(r"$v$")
        ax3.set_xlabel(r"$x$")
        ax3.xaxis.set_minor_locator(ticker.AutoMinorLocator())
        ax3.yaxis.set_minor_locator(ticker.AutoMinorLocator())
        ax3.axis(ymin=pextent_v[0],ymax=pextent_v[1])

        # Aesthetic tweaks.
        ax1.xaxis.set_tick_params(labelbottom='off')
        ax2.xaxis.set_tick_params(labelbottom='off')
        ax1.grid(True)
        ax2.grid(True)
        ax3.grid(True)

        makeplot(ax1, ax2, ax3, xextent[0], rho0, T0, theta0, v0, x_precursor_ma,
            rho_precursor, T_precursor, theta_precursor, v_precursor, x_relax_ma,
            rho_relax, T_relax, theta_relax, v_relax, xextent[1], rho_downstream,
            T_downstream, theta_downstream, v_downstream, drho_jump, dT_jump,
            dv_jump, indices, x_scale=L_tilde, rho_scale=rho_tilde, T_scale=T_tilde,
            v_scale=a_tilde)

        plt.draw()
        plt.savefig('nonequil_results.png')



