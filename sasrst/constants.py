#
"""
    List of useful physical constants in cgs units.
"""

#  radiation and atomic physics:
clght = 2.99792e10        # speed of light [cm s**-1]
sbc  = 5.670373e-5        # Stefan-Boltzmann constant [erg s**-1 cm**-2 K**-4]
arad = 4.0 * sbc / clght  # radiation constant [erg cm**-3 K**-4]
kbltz = 1.38066e-16       # Boltzmann's constant [erg/K]
mpro = 1.67265e-24        # proton mass [g]
mamu = 1.66053e-24        # atomic mass unit [g]
hplnck = 6.626069e-27     # Planck's constant [erg s]

#  astronomical:
au    = 1.49598e13        # 1 astronomical unit [cm]
msun  = 1.98892e33        # mass of the Sun [g]
rsun  = 6.9598e10         # radius of the Sun [cm]
lsun  = 3.839e33          # luminosity of the Sun [erg s**-1]
gcnst = 6.67384e-8        # gravitational constant [cm**3 g**-1 s**-2]
pc    = 3.0856776e18      # 1 parsec [cm]

#  conversion factors:
evtok = 1.1604505e9       # electron volts to Kelvin
habing = 1.6e-3           # CGS units of flux [erg cm**-3 s**-1] to Habing units [cm**-2]

