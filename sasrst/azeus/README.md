# Note #

This sub-directory contains the files for the 1-D radiative shock
tube problem which are specific to AZEuS.

That said, `reldiff.py` and `addtoplot.py` can serve as templates for
comparing the semi-analytic solutions produced by `../equilibrium.py`
and `../nonequilibrium.py` to numerical results from any code.
