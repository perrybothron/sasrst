"""
Take the results of `nonequilibrium.py` and add them to an existing (1D)
plot for the radiative shock wave problem.  This script currently requires
that `nonequilibrium.py` and has already been successfully executed, that its
variables remain available (for interactive use), and the axes/plot that
you want to add to is accessible.

Note that this short script is nearly the same as the `makeplot` function
in `nonequilibrium.py`.

"""

import numpy as np
import matplotlib.pyplot as plt


if __name__ == "__main__":

    # Specify axes that you want to add to.
    rho_ax = axes[0]
    T_ax = axes[0]
    theta_ax = axes[0]
    v_ax = axes[0]

    rho_scale = rho_tilde
    x_scale = L_tilde
    T_scale = T_tilde
    v_scale = a_tilde

    sa_marker = 'k-'
    lw = 1.5
    xoffset = 0.0 # in units of `L_tilde`.

    ntotal = 512
    dx = (xextent[1] - xextent[0]) / ntotal
    x_upstream_a = np.arange(xextent[0],x_precursor_ma.min()*x_scale,dx) + xoffset
    x_downstream_a = np.arange(x_relax_ma.max()*x_scale,xextent[1],dx) + xoffset

    # Plot precursor region.
    rho_ax.plot(x_precursor_ma*x_scale+xoffset,rho_precursor*rho_scale,sa_marker,
                lw=lw,zorder=99)
    T_ax.plot(x_precursor_ma*x_scale+xoffset,T_precursor*T_scale,sa_marker,lw=lw,zorder=99)
    theta_ax.plot(x_precursor_ma*x_scale+xoffset,theta_precursor*T_scale,sa_marker,
                lw=lw,zorder=99)
    v_ax.plot(x_precursor_ma*x_scale+xoffset,v_precursor*v_scale,sa_marker,lw=lw,zorder=99)

    # Plot relaxation region.
    rho_ax.plot(x_relax_ma*x_scale+xoffset,rho_relax*rho_scale,sa_marker,lw=lw,zorder=99)
    T_ax.plot(x_relax_ma*x_scale+xoffset,T_relax*T_scale,sa_marker,lw=lw,zorder=99)
    theta_ax.plot(x_relax_ma*x_scale+xoffset,theta_relax*T_scale,sa_marker,lw=lw,zorder=99)
    v_ax.plot(x_relax_ma*x_scale+xoffset,v_relax*v_scale,sa_marker,lw=lw,zorder=99)

    # Plot upstream region.
    rho_upstream_a = np.ones_like(x_upstream_a) * rho0 * rho_scale
    T_upstream_a = np.ones_like(x_upstream_a) * T0 * T_scale
    theta_upstream_a = np.ones_like(x_upstream_a) * theta0 * T_scale
    v_upstream_a = np.ones_like(x_upstream_a) * v0 * v_scale
    rho_ax.plot(x_upstream_a,rho_upstream_a,sa_marker,lw=lw,zorder=99)
    T_ax.plot(x_upstream_a,T_upstream_a,sa_marker,lw=lw,zorder=99)
    theta_ax.plot(x_upstream_a,theta_upstream_a,sa_marker,lw=lw,zorder=99)
    v_ax.plot(x_upstream_a,v_upstream_a,sa_marker,lw=lw,zorder=99)
    
    # Plot downstream region.
    rho_downstream_a = np.ones_like(x_downstream_a) * rho_downstream * rho_scale
    T_downstream_a = np.ones_like(x_downstream_a) * T_downstream * T_scale
    theta_downstream_a = np.ones_like(x_downstream_a) * theta_downstream * T_scale
    v_downstream_a = np.ones_like(x_downstream_a) * v_downstream * v_scale
    rho_ax.plot(x_downstream_a,rho_downstream_a,sa_marker,lw=lw,zorder=99)
    T_ax.plot(x_downstream_a,T_downstream_a,sa_marker,lw=lw,zorder=99)
    theta_ax.plot(x_downstream_a,theta_downstream_a,sa_marker,lw=lw,zorder=99)
    v_ax.plot(x_downstream_a,v_downstream_a,sa_marker,lw=lw,zorder=99)

    if theta_precursor.max() >= theta_relax.min():
        # Plot hydrodynamic shock region.
        rho_jump0 = rho_precursor[indices['th_pindx']]*rho_scale
        T_jump0 = T_precursor[indices['th_pindx']]*T_scale
        v_jump0 = v_precursor[indices['th_pindx']]*v_scale
        rho_ax.plot([0.0+xoffset,0.0+xoffset],[rho_jump0,rho_jump0*drho_jump],sa_marker,
                    lw=lw,zorder=99)
        T_ax.plot([0.0+xoffset,0.0+xoffset],[T_jump0,T_jump0*dT_jump],sa_marker,
                    lw=lw,zorder=99)
        v_ax.plot([0.0+xoffset,0.0+xoffset],[v_jump0,v_jump0*dv_jump],sa_marker,
                    lw=lw,zorder=99)

    plt.draw()
    
