"""
Calculate the relative differences between AZEuS AMR data and the
semi-analytic solutions of Lowrie & Edwards (2008) for radiative shock
waves in the non-equilibrium diffusion approximation.

"""

import numpy as np
import matplotlib.pyplot as plt
from matplotlib import ticker
import varfuncs as vf

HUGE = 1.0e99

def decimate_interpolate(xdata, xorig, qorig):
    """
    Decimate 1-D arrays `xorig` and `qorig` to the resolution of `xdata`
    using linear interpolation.

    """

    nxd = len(xdata)
    nxo = len(xorig)
    qinterp = np.zeros_like(xdata)
    qinterp = np.ma.array(qinterp,mask=False)

    ##ipdb.set_trace()
    for k in xrange(2,nxd-3):
        test = xdata[k] - xorig
        index = np.where(abs(test) == abs(test).min())[0]
        index0 = index[0]

        ##ipdb.set_trace()
        numerator = abs(test[index0])
        if test[index0] < 0 and index0 > 0:
            denominator = xorig[index0] - xorig[index0-1]
            indexa = index0
            indexb = index0 - 1
        elif test[index0] > 0 and index0 < nxo-1:
            denominator = xorig[index0+1] - xorig[index0]
            indexa = index0
            indexb = index0 + 1
        else:
            denominator = np.nan
            indexa = indexb = 0

        if denominator is np.nan:
            qinterp.mask[k] = True
        else:
            weight = numerator / denominator
            qinterp[k] = (1.0 - weight) * qorig[indexa] + weight * qorig[indexb]

    return qinterp

if __name__ == "__main__":

    pymin = [1e-6, 1e-6, 1e-6]
    pymax = [8e-1, 8e-1, 8e-1]

    # Alias for the interpolation and relative difference functions.
    de_in = decimate_interpolate
    r_d = vf.relative_difference

    # Determine grey levels for AMR grids.
    greys = amr_common.amr_shading(maxlvl-minlvl+1)

    # Do precursor region for all grids and variables.
    # Includes interpolation and calculation of relative differences.
    rho_precursor_ca = []
    T_precursor_ca = []
    theta_precursor_ca = []
    v_precursor_ca = []
    rho_precursor_rd = []
    T_precursor_rd = []
    theta_precursor_rd = []
    v_precursor_rd = []
    rho_rdmin = HUGE
    rho_rdmax = -HUGE
    T_rdmin = HUGE
    T_rdmax = -HUGE
    theta_rdmin = HUGE
    theta_rdmax = -HUGE
    v_rdmin = HUGE
    v_rdmax = -HUGE
    for ca in a:
        xa = ca.grid['x1a'].data
        xb = ca.grid['x1b'].data
        # Interpolate data to AZEuS grid.
        rho_prec_ca = de_in(xb,x_precursor_ma*L_tilde,rho_precursor)
        T_prec_ca = de_in(xb,x_precursor_ma*L_tilde,T_precursor)
        theta_prec_ca = de_in(xb,x_precursor_ma*L_tilde,theta_precursor)
        v_prec_ca = de_in(xa,x_precursor_ma*L_tilde,v_precursor)
        rho_precursor_ca.append(rho_prec_ca)
        T_precursor_ca.append(T_prec_ca)
        theta_precursor_ca.append(theta_prec_ca)
        v_precursor_ca.append(v_prec_ca)

        # Calculate relative differences.
        rho_prec_rd = r_d(rho_prec_ca*rho_tilde,ca['d'].data.squeeze())
        T_azeus = vf.gas_temperature(ca['e1'].data,ca['d'].data,common.gamma,common.mu).squeeze()
        T_prec_rd = r_d(T_prec_ca*T_tilde,T_azeus)
        theta_azeus = vf.radiation_temperature(ca['er'].data.squeeze())
        theta_prec_rd = r_d(theta_prec_ca*T_tilde,theta_azeus)
        v_prec_rd = r_d(v_prec_ca*a_tilde,ca['v1'].data.squeeze())
        rho_precursor_rd.append(rho_prec_rd)
        T_precursor_rd.append(T_prec_rd)
        theta_precursor_rd.append(theta_prec_rd)
        v_precursor_rd.append(v_prec_rd)

        rho_rdmin = min(np.nanmin(rho_prec_rd), rho_rdmin)
        rho_rdmax = max(np.nanmax(rho_prec_rd), rho_rdmax)
        T_rdmin = min(np.nanmin(T_prec_rd), T_rdmin)
        T_rdmax = max(np.nanmax(T_prec_rd), T_rdmax)
        theta_rdmin = min(np.nanmin(theta_prec_rd), theta_rdmin)
        theta_rdmax = max(np.nanmax(theta_prec_rd), theta_rdmax)
        v_rdmin = min(np.nanmin(v_prec_rd), v_rdmin)
        v_rdmax = max(np.nanmax(v_prec_rd), v_rdmax)

    print "Precursor relative difference (underlying data not excluded):"
    print "rho min.: {0:.5e}; max: {1:.5e}".format(rho_rdmin,rho_rdmax)
    print "T min.: {0:.5e}; max: {1:.5e}".format(T_rdmin,T_rdmax)
    print "theta min.: {0:.5e}; max: {1:.5e}".format(theta_rdmin,theta_rdmax)
    print "v min.: {0:.5e}; max: {1:.5e}".format(v_rdmin,v_rdmax)

    print "------------------------------------"

    # Do relaxation region.
    rho_relax_ca = []
    T_relax_ca = []
    theta_relax_ca = []
    v_relax_ca = []
    rho_relax_rd = []
    T_relax_rd = []
    theta_relax_rd = []
    v_relax_rd = []
    rho_rdmin = HUGE
    rho_rdmax = -HUGE
    T_rdmin = HUGE
    T_rdmax = -HUGE
    theta_rdmin = HUGE
    theta_rdmax = -HUGE
    v_rdmin = HUGE
    v_rdmax = -HUGE
    for ca in a:
        xa = ca.grid['x1a'].data
        xb = ca.grid['x1b'].data
        # Interpolate data to AZEuS grid.
        rho_prec_ca = de_in(xb,x_relax_ma*L_tilde,rho_relax)
        T_prec_ca = de_in(xb,x_relax_ma*L_tilde,T_relax)
        theta_prec_ca = de_in(xb,x_relax_ma*L_tilde,theta_relax)
        v_prec_ca = de_in(xa,x_relax_ma*L_tilde,v_relax)
        rho_relax_ca.append(rho_prec_ca)
        T_relax_ca.append(T_prec_ca)
        theta_relax_ca.append(theta_prec_ca)
        v_relax_ca.append(v_prec_ca)

        # Calculate relative differences.
        rho_prec_rd = r_d(rho_prec_ca*rho_tilde,ca['d'].data.squeeze())
        T_azeus = vf.gas_temperature(ca['e1'].data,ca['d'].data,common.gamma,common.mu).squeeze()
        T_prec_rd = r_d(T_prec_ca*T_tilde,T_azeus)
        theta_azeus = vf.radiation_temperature(ca['er'].data.squeeze())
        theta_prec_rd = r_d(theta_prec_ca*T_tilde,theta_azeus)
        v_prec_rd = r_d(v_prec_ca*a_tilde,ca['v1'].data.squeeze())
        rho_relax_rd.append(rho_prec_rd)
        T_relax_rd.append(T_prec_rd)
        theta_relax_rd.append(theta_prec_rd)
        v_relax_rd.append(v_prec_rd)

        rho_rdmin = min(np.nanmin(rho_prec_rd), rho_rdmin)
        rho_rdmax = max(np.nanmax(rho_prec_rd), rho_rdmax)
        T_rdmin = min(np.nanmin(T_prec_rd), T_rdmin)
        T_rdmax = max(np.nanmax(T_prec_rd), T_rdmax)
        theta_rdmin = min(np.nanmin(theta_prec_rd), theta_rdmin)
        theta_rdmax = max(np.nanmax(theta_prec_rd), theta_rdmax)
        v_rdmin = min(np.nanmin(v_prec_rd), v_rdmin)
        v_rdmax = max(np.nanmax(v_prec_rd), v_rdmax)

    print "Relaxation relative difference (underlying data not excluded):"
    print "rho min.: {0:.5e}; max: {1:.5e}".format(rho_rdmin,rho_rdmax)
    print "T min.: {0:.5e}; max: {1:.5e}".format(T_rdmin,T_rdmax)
    print "theta min.: {0:.5e}; max: {1:.5e}".format(theta_rdmin,theta_rdmax)
    print "v min.: {0:.5e}; max: {1:.5e}".format(v_rdmin,v_rdmax)

    print "------------------------------------"

    # Next: upstream and downstream regions (which are constant in space).
    ntotal = 1000
    dx = (xextent[1] - xextent[0]) / ntotal
    x_upstream_a = np.arange(xextent[0],x_precursor_ma.min()*L_tilde,dx)
    x_downstream_a = np.arange(x_relax_ma.max()*L_tilde,xextent[1],dx)

    # Generate an array for the upstream and downstream data.
    rho_upstream_a = np.ones_like(x_upstream_a) * rho0
    T_upstream_a = np.ones_like(x_upstream_a) * T0
    theta_upstream_a = np.ones_like(x_upstream_a) * theta0
    v_upstream_a = np.ones_like(x_upstream_a) * v0
    rho_downstream_a = np.ones_like(x_downstream_a) * rho_downstream
    T_downstream_a = np.ones_like(x_downstream_a) * T_downstream
    theta_downstream_a = np.ones_like(x_downstream_a) * theta_downstream
    v_downstream_a = np.ones_like(x_downstream_a) * v_downstream

    # Calculate relative differences.
    rho_upstream_ca = []
    T_upstream_ca = []
    theta_upstream_ca = []
    v_upstream_ca = []
    rho_upstream_rd = []
    T_upstream_rd = []
    theta_upstream_rd = []
    v_upstream_rd = []
    rho_rdmin = HUGE
    rho_rdmax = -HUGE
    T_rdmin = HUGE
    T_rdmax = -HUGE
    theta_rdmin = HUGE
    theta_rdmax = -HUGE
    v_rdmin = HUGE
    v_rdmax = -HUGE
    for ca in a:
        xa = ca.grid['x1a'].data
        xb = ca.grid['x1b'].data
        # Interpolate data to AZEuS grid.
        ##ipdb.set_trace()
        rho_prec_ca = de_in(xb,x_upstream_a,rho_upstream_a)
        T_prec_ca = de_in(xb,x_upstream_a,T_upstream_a)
        theta_prec_ca = de_in(xb,x_upstream_a,theta_upstream_a)
        v_prec_ca = de_in(xa,x_upstream_a,v_upstream_a)
        rho_upstream_ca.append(rho_prec_ca)
        T_upstream_ca.append(T_prec_ca)
        theta_upstream_ca.append(theta_prec_ca)
        v_upstream_ca.append(v_prec_ca)

        # Calculate relative differences.
        rho_prec_rd = r_d(rho_prec_ca*rho_tilde,ca['d'].data.squeeze())
        ##ipdb.set_trace()
        T_azeus = vf.gas_temperature(ca['e1'].data,ca['d'].data,common.gamma,common.mu).squeeze()
        T_prec_rd = r_d(T_prec_ca*T_tilde,T_azeus)
        theta_azeus = vf.radiation_temperature(ca['er'].data.squeeze())
        theta_prec_rd = r_d(theta_prec_ca*T_tilde,theta_azeus)
        v_prec_rd = r_d(v_prec_ca*a_tilde,ca['v1'].data.squeeze())
        rho_upstream_rd.append(rho_prec_rd)
        T_upstream_rd.append(T_prec_rd)
        theta_upstream_rd.append(theta_prec_rd)
        v_upstream_rd.append(v_prec_rd)

        ##ipdb.set_trace()
        if not rho_prec_rd.mask.all():
            rho_rdmin = min(np.nanmin(rho_prec_rd), rho_rdmin)
            rho_rdmax = max(np.nanmax(rho_prec_rd), rho_rdmax)
            T_rdmin = min(np.nanmin(T_prec_rd), T_rdmin)
            T_rdmax = max(np.nanmax(T_prec_rd), T_rdmax)
            theta_rdmin = min(np.nanmin(theta_prec_rd), theta_rdmin)
            theta_rdmax = max(np.nanmax(theta_prec_rd), theta_rdmax)
            v_rdmin = min(np.nanmin(v_prec_rd), v_rdmin)
            v_rdmax = max(np.nanmax(v_prec_rd), v_rdmax)

    print "Upstream relative difference (underlying data not excluded):"
    print "rho min.: {0:.5e}; max: {1:.5e}".format(rho_rdmin,rho_rdmax)
    print "T min.: {0:.5e}; max: {1:.5e}".format(T_rdmin,T_rdmax)
    print "theta min.: {0:.5e}; max: {1:.5e}".format(theta_rdmin,theta_rdmax)
    print "v min.: {0:.5e}; max: {1:.5e}".format(v_rdmin,v_rdmax)

    print "------------------------------------"

    # Calculate relative differences.
    rho_downstream_ca = []
    T_downstream_ca = []
    theta_downstream_ca = []
    v_downstream_ca = []
    rho_downstream_rd = []
    T_downstream_rd = []
    theta_downstream_rd = []
    v_downstream_rd = []
    rho_rdmin = HUGE
    rho_rdmax = -HUGE
    T_rdmin = HUGE
    T_rdmax = -HUGE
    theta_rdmin = HUGE
    theta_rdmax = -HUGE
    v_rdmin = HUGE
    v_rdmax = -HUGE
    for ca in a:
        xa = ca.grid['x1a'].data
        xb = ca.grid['x1b'].data
        # Interpolate data to AZEuS grid.
        ##ipdb.set_trace()
        rho_prec_ca = de_in(xb,x_downstream_a,rho_downstream_a)
        T_prec_ca = de_in(xb,x_downstream_a,T_downstream_a)
        theta_prec_ca = de_in(xb,x_downstream_a,theta_downstream_a)
        v_prec_ca = de_in(xa,x_downstream_a,v_downstream_a)
        rho_downstream_ca.append(rho_prec_ca)
        T_downstream_ca.append(T_prec_ca)
        theta_downstream_ca.append(theta_prec_ca)
        v_downstream_ca.append(v_prec_ca)

        # Calculate relative differences.
        rho_prec_rd = r_d(rho_prec_ca*rho_tilde,ca['d'].data.squeeze())
        ##ipdb.set_trace()
        T_azeus = vf.gas_temperature(ca['e1'].data,ca['d'].data,common.gamma,common.mu).squeeze()
        T_prec_rd = r_d(T_prec_ca*T_tilde,T_azeus)
        theta_azeus = vf.radiation_temperature(ca['er'].data.squeeze())
        theta_prec_rd = r_d(theta_prec_ca*T_tilde,theta_azeus)
        v_prec_rd = r_d(v_prec_ca*a_tilde,ca['v1'].data.squeeze())
        rho_downstream_rd.append(rho_prec_rd)
        T_downstream_rd.append(T_prec_rd)
        theta_downstream_rd.append(theta_prec_rd)
        v_downstream_rd.append(v_prec_rd)

        ##ipdb.set_trace()
        if not rho_prec_rd.mask.all():
            rho_rdmin = min(np.nanmin(rho_prec_rd), rho_rdmin)
            rho_rdmax = max(np.nanmax(rho_prec_rd), rho_rdmax)
            T_rdmin = min(np.nanmin(T_prec_rd), T_rdmin)
            T_rdmax = max(np.nanmax(T_prec_rd), T_rdmax)
            theta_rdmin = min(np.nanmin(theta_prec_rd), theta_rdmin)
            theta_rdmax = max(np.nanmax(theta_prec_rd), theta_rdmax)
            v_rdmin = min(np.nanmin(v_prec_rd), v_rdmin)
            v_rdmax = max(np.nanmax(v_prec_rd), v_rdmax)

    print "Downstream relative difference (underlying data not excluded):"
    print "rho min.: {0:.5e}; max: {1:.5e}".format(rho_rdmin,rho_rdmax)
    print "T min.: {0:.5e}; max: {1:.5e}".format(T_rdmin,T_rdmax)
    print "theta min.: {0:.5e}; max: {1:.5e}".format(theta_rdmin,theta_rdmax)
    print "v min.: {0:.5e}; max: {1:.5e}".format(v_rdmin,v_rdmax)

    figx = plt.figure('relative differences',facecolor='w',figsize=(17.4,2.6))
    figx.clf()
    axx = []
    axx.append(figx.add_subplot(1,3,1))
    axx.append(figx.add_subplot(1,3,2,sharex=axx[0]))
    axx.append(figx.add_subplot(1,3,3,sharex=axx[0]))
    figx.subplots_adjust(top=0.99, bottom=0.20,
          hspace=fprops['hspace'], left=fprops['left'], right=fprops['right'],
          wspace=fprops['wspace'])

    axx[0].axis(xmin=pxsize[0],xmax=pxsize[1])
    for cax in axx:
        idx = axx.index(cax)
        cax.axis(ymin=pymin[idx],ymax=pymax[idx])
        cax.grid(False)
        cax.set_ylabel(r"Relative difference",fontsize=14)
        cax.set_xlabel(r"$x$ [cm]")

    # Aesthetic tweaks.
    for cax in axx:
        cax.xaxis.set_minor_locator(ticker.AutoMinorLocator())
        for loc,spine in cax.spines.items():
            if loc in ['right','top']:
                spine.set_color('none')
                cax.xaxis.set_ticks_position('bottom')
                cax.yaxis.set_ticks_position('left')

    rect = []
    for ca,m in zip(a,mptr_i):
        xa = ca.grid['x1a'].data
        xb = ca.grid['x1b'].data
        nx = ca.grid.nx1
        idx = mptr_i.index(m)

        prevlvl = lvl
        lvl = ca.nodes[m-1,3]

        if lvl != 1 and prevlvl != lvl:
            greyfill = greys.next()

        if lmasklower:
        # Increase the z-order for each successive level
        # to mask data at lower levels.
            zorder = lvl/100.0 + 2.0
        else:
            zorder = None

        # Plot shaded rectangles to denote grids.
        if m != 1:
            ##greyfill = '1.0'
            for cax in axx:
                rect.append(cax.fill_between(xa[2:nx-2],y1=1e-10,y2=10.0,
                                             color=greyfill,zorder=zorder))

        # Precursor results.
        axx[0].semilogy(xb,T_precursor_rd[idx],'ro',ms=4,mew=0,lw=1.5,zorder=zorder)
        axx[0].semilogy(xb,theta_precursor_rd[idx],'bo',ms=4,mew=0,lw=1.5,zorder=zorder)
        axx[1].semilogy(xb,rho_precursor_rd[idx],'ro',ms=4,mew=0,lw=1.5,zorder=zorder)
        axx[2].semilogy(xa,v_precursor_rd[idx],'ro',ms=4,mew=0,lw=1.5,zorder=zorder)

        # Relaxation results.
        axx[0].semilogy(xb,T_relax_rd[idx],'ro',ms=4,mew=0,lw=1.5,zorder=zorder)
        axx[0].semilogy(xb,theta_relax_rd[idx],'bo',ms=4,mew=0,lw=1.5,zorder=zorder)
        axx[1].semilogy(xb,rho_relax_rd[idx],'ro',ms=4,mew=0,lw=1.5,zorder=zorder)
        axx[2].semilogy(xa,v_relax_rd[idx],'ro-',ms=4,mew=0,lw=1.5,zorder=zorder)

        # Upstream results.
        axx[0].semilogy(xb,T_upstream_rd[idx],'ro',ms=4,mew=0,lw=1.5,zorder=zorder)
        axx[0].semilogy(xb,theta_upstream_rd[idx],'bo',ms=4,mew=0,lw=1.5,zorder=zorder)
        axx[1].semilogy(xb,rho_upstream_rd[idx],'ro',ms=4,mew=0,lw=1.5,zorder=zorder)
        axx[2].semilogy(xa,v_upstream_rd[idx],'ro',ms=4,mew=0,lw=1.5,zorder=zorder)

        # Downstream results.
        axx[0].semilogy(xb,T_downstream_rd[idx],'ro',ms=4,mew=0,lw=1.5,zorder=zorder)
        axx[0].semilogy(xb,theta_downstream_rd[idx],'bo',ms=4,mew=0,lw=1.5,zorder=zorder)
        axx[1].semilogy(xb,rho_downstream_rd[idx],'ro',ms=4,mew=0,lw=1.5,zorder=zorder)
        axx[2].semilogy(xa,v_downstream_rd[idx],'ro',ms=4,mew=0,lw=1.5,zorder=zorder)

    plt.draw()
    plt.show()
    
