"""
Auxiliary functions.

"""

import numpy as np
from .. import constants as c

def relative_difference(A, B, absvalue=True):
    """
    Calculate the relative difference between two variables,
    A and B, using A as the reference value.  In other words:
    
    .. math::
      RD = | (A - B) / A |

    Written by: Jon Ramsey, March 2013.

    - A and B must be numpy arrays of the same shape.
    
    boolean 'absvalue': take the absolute value of the result
    
    """

    assert A.shape == B.shape, "A and B do not have the same shape."

    if absvalue:
        rd = np.abs( ( A - B ) / A )
    else:
        rd = ( A - B ) / A

    # Replace any infs with nans.
    rd[np.isinf(rd)] = np.nan

    return rd

def gas_temperature(nrg1, rho, gamma, mu):
    """
    Calculate the gas temperature given the density and the
    (internal) gas energy density, assuming the ideal gas law.

    Written by: Jon Ramsey, November 2012.

    nrg     numpy array of the internal energy density
    rho     numpy array of the gas density
    gamma   ratio of specific heats
    mu      mean molecular weight (unitless)
    
    """

    gamm1 = gamma - 1.0

    if (rho == 0.0).any():
        print "GASTEMP: rho = 0 somewhere!"

    t = nrg1  / rho * mu * c.mpro * gamm1 / c.kbltz

    return t
    
def radiation_temperature(erad):
    """
    Calculate the "radiation temperature" from the radiation energy
    density:
        $T_\mathrm{rad} = (E_\mathrm{rad} / a_\mathrm{rad})^{1/4}$,
    where $a_\mathrm{rad}$ is the radiation constant.

    """

    return (erad / c.arad)**0.25
