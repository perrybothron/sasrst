# Semi-Analytic Solutions for 1-D Radiative Shock Tubes #

Attempts to reproduce the results of Lowrie & Edwards (2007, Shock Waves, 16, 445; 2008, Shock Waves, 18, 129) for 1-D radiative shock tubes.  Both equilibrium and non-equilibrium solutions are included.

Dependencies:

* numpy (tested with v1.9.1)
* matplotlib (tested with v1.4.2)
* scipy (tested with v0.13.3)

### How-to run ###

Preferred:
```
#!python

ipython --pylab
In [1]: %run -i nonequilibrium.py
```

Also possible:

```
#!python

python nonequilibrium.py
```

If running non-interactively, the output is stored in `nonequil_results.png`.

The same procedure will also work with `equilibrium.py`.

### Suggested Workflow ###

Notwithstanding typos, the preferred workflow for making changes is:

1. Create new branch with a *descriptive name*; make changes.
2. Create pull request; upon approval, merge with `master` (and close branch).

----

Originally written by Jon Ramsey, Summer 2014.

A successful application of the included scripts can be seen in §4.4 of Ramsey & Dullemond ([2015, A&A, 574, A81](http://www.aanda.org/articles/aa/abs/2015/02/aa24954-14/aa24954-14.html)).

This software is offered with no claims about the suitability the software for any purpose. This software and the accompanying manuals are provided "as is" without expressed or implied warranty.

This software can be freely distributed or modified in accordance with the [GNU Public License v3](http://www.gnu.org/licenses/gpl-3.0.en.html).